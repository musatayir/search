# -*- coding: utf-8 -*-
from System.db import sqldb
import threading
import time
class paramet:
    def __init__(self,callback=None):
        self.key=True
        self.endtime=""
        self.starttime="",
        self.id=""
        self.cycle=20
        self.Callback=callback
        self.update()
    def stop(self):
        self.key=False
    def start(self):
        self.key=True
        self.run()   
    def run(self):
        if self.key:
            global t
            t=threading.Timer(self.cycle, self.start)
            t.start()
            new=time.time()
            if self.Callback!=None:# and new-time.mktime(self.endtime)<0 and new-time.mktime(self.starttime)>0:
                self.Callback()
    def update(self):
        sql="""SELECT * FROM system;"""
        Dt=sqldb().select(sql)
        Dt=Dt[0]
        yel=time.strftime('%Y-%m-%d',time.localtime(time.time()))+" "
        self.starttime=time.strptime(yel+str(Dt.get("starttime")),"%Y-%m-%d %H:%M:%S")
        self.endtime=time.strptime(yel+str(Dt.get("endtime")),"%Y-%m-%d %H:%M:%S")
        self.id=Dt.get("id")
        self.cycle=int(Dt.get("cycle"))