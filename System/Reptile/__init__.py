'''
Created on 2012-9-26

@author: Musa
''' 
#/(.*)
from urllib2 import urlopen,build_opener
from bs4 import BeautifulSoup,Comment
import os
import sys
class html:
    def __init__(self,url=None):
        self.iphone=False
        self.error=False
        if type(url)==type(""):
            self.Bs=self.GetBs(url)
        else:
            self.Bs=url
    def GetText(self,u,a):
        v=self.Bs.find(u,attrs=a)
        if v:
            return v.string
        else:
            return None
    def _GetText(self,u):
        for k in u:
            return k
    def Remove(self,t,item):
        t=str(t)
        B='>'
        A='<'+item
        while True:
            a=t.find(A)
            l=len(t)
            if a<0:
                break
            else:
                b=t[a:l].find(B)+len(B)+a                
                t=t[0:a]+t[b:l]
        return t.replace("</"+item+">","").replace('\r\n',"").replace('\n',"").replace('\t',"").replace('  ',"")
    def Removes(self,t,items):  
         for item in items:
             t=self.Remove(t, item)
         return t#.encode('GBK')
    def GetBs(self,url):
        self.Text=self.GetinnerHTML(url)        
        return BeautifulSoup(self.Text)
    def GetinnerHTML(self,url):
        innerHTML=""
        if self.iphone==False:
            try:
                request = urlopen(url)
                innerHTML = request.read()  
                request.close()
            except:
                self.error=True
        else: 
            try:
                opener=build_opener()
                opener.addheaders = [('User-agent', 'mozilla/5.0 (iphone; cpu iphone os 6_0_2 like mac os x) applewebkit/536.26 (khtml, like gecko) crios/26.0.1410.50 mobile/10a551 safari/8536.25 (5f2b12f7-b76e-42fe-9103-da281e318432)')]
                innerHTML = opener.open(url).read()
                opener.close()            
            except:
                self.error=True
                pass
        return innerHTML#.decode("utf-8")
    def FactorGetsBs(self,u,a):
        return self.Bs.findAll(u,attrs=a)
    def FactorGetBs(self,u,a):
        return self.Bs.find(u,attrs=a)
    def Delete(self,name):
        Ms = self.Bs.findAll(name)
        [m.extract() for m in Ms]
        return self.Bs
    def delete(self,Ms):
        [m.extract() for m in Ms]
    def Cleared(self):
        comments = self.Bs.findAll(text=lambda text:isinstance(text, Comment))
        [comment.extract() for comment in comments]
    def Replace(self,old,New):
        for key in old:
            self.Bs=self.Bs.replace(key,New)
    def DownloadsImgs(self,path):
        for image in self.Bs.findAll("img"):
            filename = image["src"].split("/")[-1]
            outpath = os.path.join(path, filename)
            if image["src"].lower().startswith("http"):
                urlopen(image["src"], outpath)
    def GetProperty(self,ui,name):
        if str(ui).find(name)>0:
            return ui[name]
        else:
            return False
    def gethtm(self,text):
        h=html()
        h.SetText(text)
        return h
    def SetText(self,text):
        self.Bs=BeautifulSoup(text)