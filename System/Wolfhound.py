# -*- coding: utf-8 -*-
from System.Cycle import paramet as Cyc
from System.Sources import paramet as Sou
import threading
class Wolfhound(threading.Thread):
    def __init__(self,callback,off=False):
        threading.Thread.__init__(self)
        self.Callback=callback
        if off==False:
            self.cyc=Cyc({})
            self.sou=Sou()
            self.cyc.OFF=False
        else:
            self.cyc=Cyc()  #周期参数
            self.cyc.OFF=True
            self.Sous=Sou().GetList()  #数据源
            self.start()
    def stop(self):
        for s in self.Sous:
            s.keys=False
        self.cyc.addupdate()
    def run(self):
        try:
            for s in self.Sous:
                self.sou=s
                if self.sou.keys:                   
                    self.sou.Strt(self.cyc)
            self.cyc.update()
        except:
            self.Callback({})
        self.Callback({})