# -*- coding: utf-8 -*-  
from System.db import sqldb
from System.Cycle import paramet as Cyc
from System import paramet as Sys
from System.Analysis import Analys
from System.Property import Meta
import uuid
from  System.Reptile import html
class paramet:
    def __init__(self,suo=None):
        self.keys=True
        self.key=""
        self.anti_filter=""
        self.filter=""
        self.id=""
        self.type=""
        self.name=""
        self.url=""
    def addSou(self,Url,Name,Type):
        self.url=str(Url).replace('http://', 'http://www.').replace('www.www', 'www')
        if (self.url+"//").replace("///", "")!=self.url+"//":
            self.url=(self.url+"//").replace("///", "")
        self.name=Name
        self.type=Type
        sql="SELECT id FROM sources  where url='"+self.url+"'"        
        if len(sqldb().select(sql))==0:
            sql="""INSERT INTO sources(`key`,
                    `type`,
                    `name`,
                    `url`,`filter`)  VALUES  ('"""+str(uuid.uuid1())+"','"+self.type + "','"+self.name+"','"+self.url+"','"+self.getfilter(self.url) +"');" 
            sqldb().Insert(sql, "sources")
    def getfilter(self,url):
        u=url.split("/")
        v=[]
        for i in u:
            #if i.find('.asp')>0 or i.find('.php')>0 or i.find('.aspx')>0 or i.find('.jsp')>0 :
             #   break
            if i!="":
                v.append(i)
        u=",".join(v)
        u=u.replace(",,", ",")
        u=u.replace("http:,", "")
        u=u.replace("www.", "")
        return u
    def ulinixcn(self):
        List=[{"type":"ﻛﯩﻨﻮ-ﻓﯩﻠﯩﻢ ﺗﻮﺭ ﺑﯧﻜﻪﺗﻠﯩﺮﻯ","url":"http://www.ulinix.cn/tur_7.html"},
              {"type":"ﺋﯘﻧﯩﯟﯦﺮﺳﺎﻝ ﺑﯧﻜﻪﺗﻠﻪﺭ","url":"http://www.ulinix.cn/tur_1.html"},
              {"type":"ﻣﯘﻧﺎﺯﯨﺮﻩ ﻣﯘﻧﺒﯩﺮﻯ","url":"http://www.ulinix.cn/tur_12.html"},
              {"type":"ﻧﺎﺧﺸﺎ ﻣﯘﺯﯨﻜﺎ ﺗﻮﺭ ﺑﯧﻜﻪﺗﻠﯩﺮﻯ","url":"http://www.ulinix.cn/tur_2.html"},
              {"type":"MTV ﺗﻮﺭ ﺑﯧﻜﻪﺗﻠﯩﺮﻯ","url":"http://www.ulinix.cn/tur_6.html"},
              {"type":"ﻣﯩﻜﺮﻭ ﺑﻠﻮﮔﻼﺭ","url":"http://www.ulinix.cn/tur_18.html"},
              {"type":"ﺑﻠﻮﮒ ﯞﻩ ﺷﻪﺧﺴﯩﻲ ﺑﻪﺗﻠﻪﺭ","url":"http://www.ulinix.cn/tur_13.html"},
              {"type":"ﺗﻮﺭ ﺋﯘﻳﯘﻧﻠﯩﺮﻯ","url":"http://www.ulinix.cn/tur_9.html"},
              {"type":"ﻛﻪﺳﭙﻰ ﺗﻮﺭ ﺑﯩﻜﻪﺗﻠﻪﺭ","url":"http://www.ulinix.cn/tur_15.html"},
              {"type":"ﺧﻪﯞﻩﺭ ﺗﻮﺭ ﺑﯩﻜﻪﺗﻠﯩﺮﻯ","url":"http://www.ulinix.cn/tur_11.html"},
              {"type":"ﻣﺎﺋﺎﺭﯨﭗ -ﻣﻪﻛﺘﻪﭖ ﺗﻮﺭ ﺑﯩﻜﻪﺗﻠﯩﺮﻯ","url":"http://www.ulinix.cn/tur_16.html"},
              {"type":"ﺋﻮﺭﮔﺎﻥ ﺷﯩﺮﻛﻪﺕ ﻛﺎﺭﺧﺎﻧﯩﻼﺭ","url":"http://www.ulinix.cn/tur_3.html"}
              ]
        for l in List:
            h=html(l.get("url"))
            h.Bs=h.FactorGetBs("div", {"class":"m"})
            As=h.FactorGetsBs("a", {})
            for a in As:
                self.addSou(a.get("href"),a.string,l.get("type"))
    def ulinixcom(self):
        List=[{"type":"ﻛﯩﻨﻮ-ﻓﯩﻠﯩﻢ ﺗﻮﺭ ﺑﯧﻜﻪﺗﻠﯩﺮﻯ","url":"http://ulinix.com/tur_6.html"},
              {"type":"ﺋﯘﻧﯩﯟﯦﺮﺳﺎﻝ ﺑﯧﻜﻪﺗﻠﻪﺭ","url":"http://ulinix.com/tur_1.html"},
              {"type":"ﻣﯘﻧﺎﺯﯨﺮﻩ ﻣﯘﻧﺒﯩﺮﻯ","url":"http://ulinix.com/tur_2.html"},
              {"type":"ﻧﺎﺧﺸﺎ ﻣﯘﺯﯨﻜﺎ ﺗﻮﺭ ﺑﯧﻜﻪﺗﻠﯩﺮﻯ","url":"http://ulinix.com/tur_4.html"},
              {"type":"MTV ﺗﻮﺭ ﺑﯧﻜﻪﺗﻠﯩﺮﻯ","url":"http://ulinix.com/tur_3.html"},
              {"type":"ﻣﯩﻜﺮﻭ ﺑﻠﻮﮔﻼﺭ","url":"http://ulinix.com/tur_25.html"},
              {"type":"ﺗﻮﺭ ﺋﯘﻳﯘﻧﻠﯩﺮﻯ","url":"http://ulinix.com/tur_7.html"},
              {"type":"ﻛﻪﺳﭙﻰ ﺗﻮﺭ ﺑﯩﻜﻪﺗﻠﻪﺭ","url":"http://ulinix.com/tur_22.html"},
              {"type":"ﺧﻪﯞﻩﺭ ﺗﻮﺭ ﺑﯩﻜﻪﺗﻠﯩﺮﻯ","url":"http://ulinix.com/tur_19.html"},
              {"type":"ﻣﺎﺋﺎﺭﯨﭗ -ﻣﻪﻛﺘﻪﭖ ﺗﻮﺭ ﺑﯩﻜﻪﺗﻠﯩﺮﻯ","url":"http://ulinix.com/tur_13.html"},
              {"type":"ﺋﻮﺭﮔﺎﻥ ﺷﯩﺮﻛﻪﺕ ﻛﺎﺭﺧﺎﻧﯩﻼﺭ","url":"http://ulinix.com/tur_15.html"},
              {"type":"ئۇچۇرلار","url":"http://ulinix.com/tur_5.html"},
              {"type":"يۇمشاق دىتاللار","url":"http://ulinix.com/tur_9.html"},
              {"type":"ساقلىق ساقلاش","url":"http://ulinix.com/tur_12.html"},
              {"type":"ئەدەبىيات - سەنئەت","url":"http://ulinix.com/tur_14.html"},
              {"type":"ئتور مۇلازىمىتى","url":"http://ulinix.com/tur_21.html"},
              {"type":"چولپانلار","url":"http://ulinix.com/tur_16.html"},
              {"type":"تور سودا تورى","url":"http://ulinix.com/tur_24.html"},
              {"type":"تور سودا تورى","url":"http://ulinix.com/tur_24.html"}
              ]
        for l in List:
            h=html(l.get("url"))
            h.Bs=h.FactorGetBs("div", {"id":"tur"})
            As=h.FactorGetsBs("a", {})
            for a in As:
                self.addSou(a.get("href"),a.string,l.get("type"))
    def Strt(self,cyc):
        self.cyc=cyc
        self.gethtml(self.url,self.url)
    def gethtml(self,url,p):
        htm=html(str(url))
        if htm.error:
            self.cyc.adderror(url)
        else:
            A=Analys(htm,self)
            htm=A.htm            
            AS=A.GetList()
            try:
                Meta(htm,url,self,self.cyc,p)
            except:
                print "Meta:----"+url
            for a in AS:
                try:
                    if A.GetUrloff(a)==True and self.keys:
                        self.gethtml(a,url)
                except:
                    print a
    def GetList(self):
        sql="""SELECT * FROM sources where stop>0  ORDER BY  url  DESC;"""
        D=sqldb().select(sql)
        List=[]
        for d in D:
            p=paramet()
            p.name=d.get("name")
            p.filter=d.get("filter")            
            p.anti_filter=d.get("anti_filter")
            p.key=d.get("key")
            p.id=d.get("id")
            p.url=d.get("url")
            p.type=d.get("type")
            p.anti_filter=p.anti_filter.split(',')
            p.filter=p.filter.split(',')
            List.append(p)
        return List
if __name__ == "__main__":
    #paramet().ulinixcn()
    paramet().ulinixcom()