'''
Created on 2012-10-1

@author: Musa
'''
# -*- coding: utf-8 -*-
import os
from System.db import sqldb
class Meta:
    def __init__(self,htm,url,suo,cyc,parint):
        self.off=True
        ms=htm.FactorGetsBs("meta",{})
        attribute={}
        for m in ms:
            if htm.GetProperty(m,"name"):
                name=htm.GetProperty(m,"name")
                content=htm.GetProperty(m,"content")
                attribute[name]=content
        title=htm.GetText("title",{})
        if title==None:
            title=""
        attribute["title"]=title        
        attribute["key"]=suo.key 
        attribute["parent"]=parint
        attribute["Coding"]=cyc.Coding
        if len(attribute)>0:
            content=str(htm.Bs).replace("'", '"').encode('utf-8')
            attribute["content"]=""
            attribute["url"]=url
            id=sqldb().Insert(self.getsql(attribute), "meta")
            sql="INSERT INTO contents(`meta`,`html`)  value  ("+str(id)+",'"+content+"');"
            #id=sqldb().Insert(sql, "contents")           
            cyc.addupdate(url)
    def getsql(self,attribute):
            sql="""INSERT INTO meta(
            `copyright`,
            `Charset`,
            `expires`,
            `Rating`,
            `Distribution`,
            `page-topic`,
            `Revisit-after`,
            `robots`,
            `discription`,
            `keywords`,
            `url`,`title`,`key`,`Coding`,`parent`)
            VALUES
            (
            '{copyright}',
            '{charset}',
            '{expires}',
            '{rating}',
            '{distribution}',
            '{page-topic}',
            '{revisit-after}',
            '{robots}',
            '{discription}',
            '{keywords}',
            '{url}','{title}','{key}','{Coding}','{parent}');"""
            for k in attribute: 
                v=attribute.get(k)
                sql=sql.replace("{"+k+"}",v)
            ar=['{copyright}','{charset}','{expires}','{rating}','{distribution}','{page-topic}','{revisit-after}','{robots}','{discription}','{keywords}','{url}','{title}','{content}','{key}','{Coding}','{parent}']
            for a in ar:
                sql=sql.replace(a,"")
            return sql
    def save(self,htm,url):
            save=False
            if save:
                path = "List"
                new_path = os.path.join(path, self.getfile(url))
                if not os.path.isdir(new_path):
                    os.makedirs(new_path)
                p=new_path+self.name+".html"
                file_obj=open(p,'w')
                file_obj.write(str(htm.Bs))
                file_obj.close()
    def getfile(self,url):
        f=url.replace('http://', '')
        f=f.replace('/www', 'www')
        f=f.split("/")
        if len(f)>1:
            i=len(f)-1
            self.name=f[i].split('.')[0]+"s"
            f[i]=''        
        k="\\".join(f)
        return k