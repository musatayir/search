﻿#coding=utf-8
#python setup.py py2exe --bundle 1
from distutils.core import setup 
from glob import glob
import py2exe 
import os, sys
import shutil
if len(sys.argv) == 1:
    sys.argv.append("py2exe")
setup(console=['Search.py' ],
      version = "0.1",
      description = u'ئىزدەش ماتورى',
      name = "Search",
      data_files=["Search.ico","Search.png"])
os.remove("dist/w9xpopen.exe")
shutil.rmtree("build")